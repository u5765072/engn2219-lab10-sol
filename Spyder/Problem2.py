import numpy as np
import matplotlib.pyplot  as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def _test(x, y, a):
    """
    Test if point lies within the inner region of the lemniscate
    """
    return (x**2 + y**2)**2 - 2 * a**2 * (x**2 - y**2) < 0

def random_pair_gen(a, n):
    '''a generator which returns n pairs of uniformly
    distributed pseudo-random numbers, where the interval
    for x lies within:
    [0, a * sqrt(2))
    
    And the interval for y lies within:
    [0, a/2)
    
    Input:
    a: a constant
    n: total number of pairs
    '''
    i = 0
    while i < n:
        yield np.array([np.random.uniform(low=0,high= a*np.sqrt(2)), np.random.uniform(low=0,high=a/2)])
        i += 1

def calculate_lemn_area(a, n):
    """calculates lemniscate area using Monte Carlo method using a generator 
	and avoiding storing all sample values in memory
    
    Input:
    a: a constant
    n: total number of pairs
    """
    ins = 0
    for x,y in random_pair_gen(a, n):
        if _test(x,y,a): ins += 1
    
    # number of points within the inner region vs total number of points
    ratio = ins / n
    area = ratio * a * np.sqrt(2) * a / 2

    return 4 * area


def compare_approx_area(a, n):
    """
    Given a constant "a", we could approximate the lemniscate with formula: 2 * a**2
    
    Return the absolute difference between the formula above and the monte carlo method
    and round it to 5 decimals
    """
    area = 2 * a**2
    
    approx_area = calculate_lemn_area(a,n)
    
    return np.abs(round(area - approx_area, 5)), approx_area


# Feel free to change the a and n values
diff, approx_area = compare_approx_area(a=3, n=1000)
print(f"The approximated area using monte carlo is {approx_area}")
print(f"The difference between the approx are and 2a^2 is {diff}")