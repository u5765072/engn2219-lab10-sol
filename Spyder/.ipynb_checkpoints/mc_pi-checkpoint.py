#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Computing area-like values, including the number \u03c0 using
the Monte Carlo method has low precision and poor convergence (to achieve
precision \u03b5 it requires n \u221d \u039f(1/\u03b5 ^ 2) steps to run --
that's why pseudo-random generators must have good random properties AND
high productivity). But sometimes it is the only method available'''

"Comments added"

import numpy as np

def _test(x,y):
    return x**2 + y**2 < 1

def random_pair_gen(n=1000):
    '''a generator which returns n pairs of uniformly
    distributed pseudo-random numbers in (0,1) interval'''
    i = 0
    while i < n:
        yield np.random.random_sample((2,))
        i += 1



def calculate_pi(n=1000):
    """calculates pi using Monte Carlo method with n runs
	using numpy array and numpy loop-less operation"""
    points =  np.random.rand(n,2)
    x,y = points[:,0], points[:,1]
    ## uncomment the following lines one by one to help understand the code
#     print(points)
#     print(x.shape)
#     print(y.shape)
#     print(_test(x,y))
#     print(_test(x,y).shape)
#     print(x[_test(x,y)])
#     print(x[_test(x,y)].shape)
#     return 4*(x[_test(x,y)].size)/n
    return 4 * sum(_test(x,y)) / n  # Alternative code to the line above

# Some code to help understand calculate_pi
x = np.array([1,2,3,4,5])
y = np.array([True, False, True, True, False])
z = True + False + True

print(f"x[y]: \n{x[y]}\n")
print(f"z: \n{z}\n")


def calculate_pi_with_gen(n=1000):
    """calculates pi using Monte Carlo method using a generator 
	and avoiding storing all sample values in memory"""
    ins = 0
    for x,y in random_pair_gen(n):
        if _test(x,y): ins += 1
            
    print(ins)
    return 4 * ins / n


def calculate_pi_with_gen(n=1000):
    """calculates pi using Monte Carlo method using a generator 
	and avoiding storing all sample values in memory"""
    ins = 0
    for x,y in random_pair_gen(n):
        if _test(x,y): ins += 1
    return 4 * ins / n


if __name__ == '__main__':

    import sys

    try:
        n = int(sys.argv[1])  # number of steps in Brownina motion
    except Exception as ex:
        n = 5000
        print(f'No valid value provided, using default number of steps {n}')

    res = calculate_pi_with_gen(n)
    print(f'Monte Carlo with {n} runs gives \u03c0 \u2245 {res:.5f}')
