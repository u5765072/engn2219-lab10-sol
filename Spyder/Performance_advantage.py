import numpy as np
import time

def f(x): 
    return x**2 - 3*x + 4 

# Python
x = np.arange(1e6)
y = []
start = time.perf_counter()
for i in x:
    y.append(f(i))
    
time_py = time.perf_counter() - start
print(f"Time usage for python {time_py}")

# Numpy
x = np.arange(1e6)
start = time.perf_counter()
y = f(x)
time_np = time.perf_counter() - start
print(f"Time usage for python {time_np}")
print("Numpy goes brrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr")